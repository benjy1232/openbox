#!/bin/bash
compton -b --vsync opengl --blur-background --backend glx &
xfce4-panel -d &
feh --bg-scale ~/Pictures/wallpaper.jpg &
xrandr --output VGA-1 --auto &
xrandr --output VGA-1 --primary &
xrandr --output LVDS-1 --off &
megasync
